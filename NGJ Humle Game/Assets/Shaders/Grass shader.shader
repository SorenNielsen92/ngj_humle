﻿Shader "NGJ/Grass shader"
{
    Properties
    {
        _Color1 ("Color1", Color) = (1,1,1,1)
        _Color2 ("Color1", Color) = (0,0,0,0)
        _FlowPower ("Flow Power", range(0.1, 4)) = 1
        _FlowScale ("Flow Scale", range(0.1, 4)) = 1
        _WindSpeed ("Wind Speed", range(0.1, 10)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        Cull off
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "noice.cginc"
            #pragma multi_compile_instancing

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            fixed4 _Color1;
            fixed4 _Color2;
            float _FlowPower;
            float _FlowScale;
            float _WindSpeed;

            uniform float4 CHARACTER_LOCATION;
            uniform float4 SHEEP_POS[20];
            uniform int SHEEPS;

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                o.uv = float3(0,0,0);
                float4 worldPos = mul(unity_ObjectToWorld, fixed4(0,0,0,1));
                o.uv.x = frac(worldPos.x*32+1*worldPos.y+worldPos.z*2);
                o.uv.y = (v.vertex.z)*_FlowScale;
                float noise = cnoise(float2(worldPos.x, worldPos.z)+_Time.x*_WindSpeed);

                float4 worldPixelPos = mul(unity_ObjectToWorld, v.vertex);

                float4 transformedWorldPos = worldPixelPos;
                transformedWorldPos.x += _FlowPower * clamp(noise,-1,1) * o.uv.y;
                transformedWorldPos.y -= _FlowPower * clamp(noise,-1,1) * o.uv.y * 0.5;
                

                CHARACTER_LOCATION.y = 0;
                worldPixelPos.y = 0;

                float WalkAvoidance = clamp(1- distance(CHARACTER_LOCATION, worldPixelPos)/1.5,0,1);

                transformedWorldPos.xz += normalize((worldPixelPos.xz-CHARACTER_LOCATION.xz)) * WalkAvoidance * o.uv.y;
                transformedWorldPos.y -= WalkAvoidance * o.uv.y;



                for (int i = 0; i < SHEEPS; i++)
                {
                    SHEEP_POS[i].y = 0;
                    WalkAvoidance = clamp(1- distance(SHEEP_POS[i], worldPixelPos)/1.5,0,1);

                    transformedWorldPos.xz += normalize((worldPixelPos.xz-SHEEP_POS[i].xz)) * WalkAvoidance * o.uv.y;
                    transformedWorldPos.y -= WalkAvoidance * o.uv.y * 2;
                }


                v.vertex = mul(unity_WorldToObject, transformedWorldPos);
                o.vertex = UnityObjectToClipPos(v.vertex);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = lerp(_Color1, _Color2, i.uv.x);
                col = lerp(col, col*i.uv.y,0.1);
                return col;
            }
            ENDCG
        }
    }
}
