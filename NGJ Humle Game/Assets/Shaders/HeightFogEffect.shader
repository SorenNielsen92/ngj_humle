﻿Shader "NGJ/HeightFogEffect"
{
    Properties
    {
        _Color1 ("Color1", Color) = (1,1,1,1)
        _Color2 ("Color2", Color) = (1,1,1,1)
        _Color3 ("Color3", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
      //  Cull front 
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "noice.cginc"
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 worldPos : TEXCOORD0;
            };

            fixed4 _Color1;
            fixed4 _Color2;
            fixed4 _Color3;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex)*.2;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float data = cnoise(float2(i.worldPos.x *1.5, i.worldPos.z*7+_Time.y+clamp(sin(_Time.x),0,1)));

                data = clamp(data,0,1);

                fixed4 col = lerp(_Color1, _Color2, clamp(data+.5,0,1));
                col = lerp(col, _Color3, clamp((data-.5) * 2,0,1));
                col = lerp(col,_Color2, clamp(i.vertex.w * 0.025,0,1));
                return col;
            }
            ENDCG
        }
    }
}
