﻿Shader "NGJ/WaveShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color1 ("Color 1", Color) = (1,1,1,1)
        _Color2 ("Color 2", Color) = (0,0,0,0)
        _TexturePower ("Texture Power", Range(0,1)) = 0.5
        _PeakPower ("Peak Power", Range(0,1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "noice.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float colorLerp : TEXCOORD1;
                float height : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color1;
            fixed4 _Color2;
            float _TexturePower;
            float _PeakPower;

            v2f vert (appdata v)
            {
                v2f o;
                float4 worldPos = mul(unity_ObjectToWorld, float4(0,0,0,1));
                float4 worldPixelPos = mul(unity_ObjectToWorld, v.vertex);
                float noiceY = cnoise(float2(worldPos.z, worldPos.z)+_Time.y * .3);
                float noiceX = cnoise(float2(worldPos.z, worldPos.z)+_Time.y *.5);
                v.vertex.y += (noiceY -.2) * .5;
                v.vertex.x += (noiceX -.5) * .4;
                o.vertex = UnityObjectToClipPos(v.vertex);

                o.uv = float2(worldPixelPos.x,worldPixelPos.y) * .01;
                o.colorLerp = cnoise(float2(worldPixelPos.x, worldPixelPos.z)*0.1);
                o.height = distance(0, v.vertex.y);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 cloud = tex2D(_MainTex, i.uv);
                cloud = 0.2126*cloud.r + 0.7152*cloud.g + 0.0722*cloud.b;
                fixed4 col = lerp(_Color1, _Color2, i.colorLerp);
                col = lerp(col, col*cloud, _TexturePower);
                col = lerp(col, col +i.height, _PeakPower);
                return col;
            }
            ENDCG
        }
    }
}
