﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CharacterMovement : MonoBehaviour
{
    [SerializeField]
    private float velocity = 6.0f;

    [SerializeField]
    private float maxVelocity = 10.0f;

    [SerializeField]
    private float smoothing = 8.0f;

    private Vector3 movement;
    private Vector3 previousMovement;

    private float modifier = 1.0f;

    private Rigidbody body;
    private CharacterAnimationController animController;

    public bool Locked
    {
        get;
        set;
    }

    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        animController = GetComponentInChildren<CharacterAnimationController>();
    }

    private void LateUpdate()
    {
        movement = GetSmoothedMovement();
        previousMovement = movement;

        movement *= velocity;

        if (movement.magnitude > maxVelocity)
        {
            movement = Vector3.ClampMagnitude(movement, maxVelocity);
        }
        else if (movement.magnitude <= 0.05f)
        {
            movement = Vector3.zero;
        }
    }

    private void FixedUpdate()
    {
        if (movement.sqrMagnitude <= 0.01f)
        {
            body.MovePosition(body.position);
            return;
        }

        animController.UpdateCharacterDirection(movement);
        body.MovePosition(body.position + movement * modifier * Time.fixedDeltaTime);
    }

    public void Move(Vector3 move)
    {
        move = new Vector3(move.x, 0.0f, move.z);
        movement = move;
    }

    public void SetMoveModifier(float mod)
    {
        modifier = mod;
    }

    private Vector3 GetSmoothedMovement()
    {
        Vector3 currentMovement = Vector3.zero;

        if (!Locked)
        {
            currentMovement = movement;

            if (currentMovement.magnitude < 0.1f)
            {
                animController.UpdateCharacterDirection(currentMovement);
                return Vector3.zero;
            }
        }

        if (currentMovement.magnitude > 1.0f)
        {
            return Vector3.Lerp(previousMovement, currentMovement, Time.deltaTime * smoothing).normalized;
        }
        else
        {
            return Vector3.Lerp(previousMovement, currentMovement, Time.deltaTime * smoothing);
        }
    }
}