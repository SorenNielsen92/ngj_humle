﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellInvoker : MonoBehaviour
{
    private void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                SheepManager.instance.CreateFearZone(hit.point, 6f);
            }
        }
    }
}
