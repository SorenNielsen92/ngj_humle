﻿using System;
using UnityEngine;

public class PlayerAbilityFireCracker : MonoBehaviour 
{
	[SerializeField]
	private FireCracker fireCrackerPrefab;

	[SerializeField]
	private LayerMask groundPlane;

	private CharacterAnimationController animController;
	private AnimationEventController eventController;
	private CharacterMovement movement;

	private Vector3 targetPoint;
	private bool active;

	private void Start()
	{
		animController = GetComponentInChildren<CharacterAnimationController>();
		eventController = GetComponentInChildren<AnimationEventController>();
		movement = GetComponent<CharacterMovement>();

		eventController.OnPlayerAnimationEvent += OnPlayerAnimationEvent;
	}

	private void OnPlayerAnimationEvent(PlayerAnimationEvent obj)
	{
		switch (obj)
		{
			case PlayerAnimationEvent.ThrowStart:
				break;

			case PlayerAnimationEvent.ThrowMiddle:
				FireCracker fireCracker = Instantiate(fireCrackerPrefab, transform.position + Vector3.up, Quaternion.identity);
				fireCracker.Launch(targetPoint, groundPlane);
				break;

			case PlayerAnimationEvent.ThrowEnd:
                movement.Locked = false;
				active = false;
				break;
		}
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0) && !active)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, groundPlane))
			{
				active = true;
				movement.Locked = true;

				targetPoint = hit.point;

				animController.Anim.SetTrigger("Throw");
				animController.UpdateCharacterDirection((hit.point - transform.position).normalized);
			}
		}
	}
}