﻿using UnityEngine;
using DG.Tweening;

public class FireCracker : MonoBehaviour
{
    public float height = 4;
    public float gravity = -27;

    public float explosionRadius = 3.0f;
    public float explosionDelay = 1.0f;

    private Rigidbody body;
    private AnimationEventController eventController;
    private Animator anim;

    private LayerMask groundMask;
    private bool hitGround;

    private Tween tween;

    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();

        eventController = GetComponentInChildren<AnimationEventController>();
        eventController.OnPugaAnimationEvent += OnPugaAnimationEvent;

    }

    private void OnPugaAnimationEvent(PugaAnimationEvent obj)
    {
        switch (obj)
        {
            case PugaAnimationEvent.Bark:
                SheepManager.instance.CreateFearZone(transform.position, explosionRadius);
                break;
        }
    }

    public void Launch(Vector3 target, LayerMask ground)
    {
        Physics.gravity = Vector3.up * gravity;
        body.useGravity = true;
        body.velocity = CalculateLaunchData(target).initialVelocity;

        body.DORotate(new Vector3(0.0f, 0.0f, (Random.Range(0, 2) * 2 - 1)) * 360.0f, 1.0f, RotateMode.LocalAxisAdd).SetEase(Ease.OutSine);

        groundMask = ground;

        if (body.velocity.x < 0.0f)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180.0f, transform.eulerAngles.z);
        }
    }

    private LaunchData CalculateLaunchData(Vector3 target)
    {
        float displacementY = target.y - transform.position.y;
        Vector3 displacementXZ = new Vector3(target.x - transform.position.x, 0, target.z - transform.position.z);
        float time = Mathf.Sqrt(-2 * height / gravity) + Mathf.Sqrt(2 * (displacementY - height) / gravity);
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * height);
        Vector3 velocityXZ = displacementXZ / time;

        return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (hitGround)
        {
            return;
        }

        if (Contains(groundMask, collision.gameObject.layer))
        {
            hitGround = true;
            anim.SetTrigger("Bark");

            SpriteRenderer render = GetComponentInChildren<SpriteRenderer>();
            tween = render.DOFade(0.0f, 1.0f).SetDelay(2.0f).SetEase(Ease.Linear).OnComplete(() =>
            {
                if (gameObject.activeInHierarchy)
                {
                    Destroy(gameObject);
                }
            });
        }
    }

    private void OnDestroy()
    {
        if (tween != null)
        {
            tween.Kill();
        }
    }

    private bool Contains(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}

struct LaunchData
{
    public readonly Vector3 initialVelocity;
    public readonly float timeToTarget;

    public LaunchData(Vector3 initialVelocity, float timeToTarget)
    {
        this.initialVelocity = initialVelocity;
        this.timeToTarget = timeToTarget;
    }
}