﻿using UnityEngine;

public class PlayerInput : MonoBehaviour 
{
	private CharacterMovement moveController;

	private void Awake()
	{
		moveController = GetComponent<CharacterMovement>();
	}

	private void Update()
	{
		Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical"));
		moveController.Move(input);

		Shader.SetGlobalVector("CHARACTER_LOCATION", transform.position);
	}
}