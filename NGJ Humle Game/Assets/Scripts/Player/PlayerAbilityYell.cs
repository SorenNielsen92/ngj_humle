﻿using UnityEngine;

public class PlayerAbilityYell : MonoBehaviour
{
    [SerializeField]
    private KeyCode inputKey = KeyCode.E;

    [SerializeField]
    private float cooldown = 1.0f;

    [SerializeField]
    private float radius = 3.0f;

    [SerializeField]
    private bool debug = true;

    [SerializeField]
    private ParticleSystem yellParticle;

    private Animator anim;
    private CharacterMovement movement;
    private AnimationEventController eventController;

    private bool active = false;

    private void Awake()
    {
        movement = GetComponent<CharacterMovement>();
        anim = GetComponentInChildren<Animator>();
        eventController = anim.GetComponent<AnimationEventController>();
        eventController.OnPlayerAnimationEvent += OnAnimationEvent;
    }

    private void Update()
    {
        if (Input.GetKey(inputKey) && !active)
        {
            TriggerAbility();
        }
        else if (Input.GetKeyUp(inputKey))
        {
            EndAbility();
        }
    }

    private void TriggerAbility()
    {
        active = true;
        anim.SetBool("Yell", true);
        yellParticle.Play(true);
    }

    private void EndAbility()
    {
        anim.SetBool("Yell", false);
        active = false;

        yellParticle.Stop();
    }

    private void OnAnimationEvent(PlayerAnimationEvent obj)
    {
        switch (obj)
        {
            case PlayerAnimationEvent.YellBegin:
                movement.Locked = true;
                break;

            case PlayerAnimationEvent.YellMiddle:
                SheepManager.instance.CreateFearZone(transform.position, radius);
                CameraController.Instance.Shake(ShakeStrength.Tiny);
                break;

            case PlayerAnimationEvent.YellEnd:
                movement.Locked = false;
                break;
        }
    }

    private void OnDrawGizmos()
    {
        if (debug)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, radius);
        }
    }
}