﻿using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour 
{
	public static ScoreManager Instance;

	[SerializeField]
	private TextMeshProUGUI sheepAliveField;

	[SerializeField]
	private TextMeshProUGUI sheepSavedField;

	public int SheepAlive
	{
		get;
		private set;
	}
	public int SheepSaved
	{
		get;
		private set;
	}

	private void Awake()
	{
		Instance = this;

		GlobalEvents.OnSheepDied += GlobalEvents_OnSheepDied;
		GlobalEvents.OnSheepSaved += GlobalEvents_OnSheepSaved;
	}

	private void Start()
	{
		SheepAlive = SheepManager.instance.sheeps.Count;

		UpdateSheepAliveField();
		UpdateSheepSavedField();
	}

	private void GlobalEvents_OnSheepDied(Sheep sheep)
	{
		SheepAlive--;
		UpdateSheepAliveField();
	}

	private void GlobalEvents_OnSheepSaved(Sheep sheep)
	{
		SheepSaved++;
		UpdateSheepSavedField();
	}

	private void UpdateSheepAliveField()
	{
		sheepAliveField.text = $"Unisheep Alive: {SheepAlive}";
	}

	private void UpdateSheepSavedField()
	{
		sheepSavedField.text = $"Unisheep Saved: {SheepSaved}";
	}
}