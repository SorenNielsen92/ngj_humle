﻿using UnityEngine;

public class ScoreGate : MonoBehaviour
{
    [SerializeField]
    private BoxCollider restingArea;

    [SerializeField]
    private GameObject nakedSheep;

    private Vector3 RandomPointInBounds(Bounds bounds)
    {
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }

    public void SpawnNakedSheep()
    {
        Vector3 randomPos = RandomPointInBounds(restingArea.bounds);
        randomPos.y = 0.0f;

        Instantiate(nakedSheep, randomPos, Quaternion.identity);
    }
}