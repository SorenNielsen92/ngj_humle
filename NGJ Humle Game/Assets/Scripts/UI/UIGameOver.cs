﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIGameOver : MonoBehaviour 
{
	private CanvasGroup mainCanvasGroup;

	[SerializeField]
	private CanvasGroup gameOverCanvas;

	[SerializeField]
	private TMPro.TextMeshProUGUI failDescriptionField;

	private void Awake()
	{
		mainCanvasGroup = GetComponent<CanvasGroup>();
		mainCanvasGroup.alpha = 0.0f;
		mainCanvasGroup.interactable = false;

		gameOverCanvas.alpha = 0.0f;
		gameOverCanvas.interactable = false;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SceneManager.LoadScene(0, LoadSceneMode.Single);
		}

		if (!mainCanvasGroup.interactable && SheepManager.instance.sheeps.Count == 0)
		{
			ShowGameOver("All the unisheep were lost");
		}
	}

	public void TryAgain()
	{
		SceneManager.LoadScene(1, LoadSceneMode.Single);
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	public void ShowGameOver(string failDescription)
	{
		mainCanvasGroup.interactable = true;
		mainCanvasGroup.alpha = 1.0f;

		failDescriptionField.text = failDescription;

		ScoreManager.Instance.gameObject.SetActive(false);
		FindObjectOfType<UIWinScreen>().gameObject.SetActive(false);

		PlayerInput player = FindObjectOfType<PlayerInput>();
		player.enabled = false;

		player.GetComponent<CharacterMovement>().Move(Vector3.zero);
		player.GetComponent<PlayerAbilityYell>().enabled = false;
		player.GetComponent<PlayerAbilityFireCracker>().enabled = false;

		gameOverCanvas.interactable = true;

		gameOverCanvas.DOFade(1.0f, 1.0f)
			.SetEase(Ease.OutSine);
	}
}