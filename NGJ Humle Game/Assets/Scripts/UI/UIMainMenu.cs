﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMainMenu : MonoBehaviour 
{
	public void StartGame()
	{
		SceneManager.LoadScene(1, LoadSceneMode.Single);
	}

	public void QuitGame()
	{
		Application.Quit();
	}
}