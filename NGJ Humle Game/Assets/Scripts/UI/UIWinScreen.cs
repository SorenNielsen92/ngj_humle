﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIWinScreen : MonoBehaviour 
{
	private CanvasGroup mainCanvasGroup;

	[SerializeField]
	private CanvasGroup winCanvas;

	[SerializeField]
	private TMPro.TextMeshProUGUI descriptionField;

	private int initSheep;
	private bool anySheepSaved;

	private void Awake()
	{
		mainCanvasGroup = GetComponent<CanvasGroup>();
		mainCanvasGroup.alpha = 0.0f;
		mainCanvasGroup.interactable = false;

		winCanvas.alpha = 0.0f;
		winCanvas.interactable = false;

		GlobalEvents.OnSheepSaved += GlobalEvents_OnSheepSaved;
	}

	private void GlobalEvents_OnSheepSaved(Sheep sheep)
	{
		anySheepSaved = true;
	}

	private void Start()
	{
		initSheep = SheepManager.instance.sheeps.Count;
	}

	private void Update()
	{
		if (anySheepSaved && !winCanvas.interactable && ScoreManager.Instance.SheepSaved == SheepManager.instance.sheeps.Count)
		{
			mainCanvasGroup.interactable = true;
			mainCanvasGroup.alpha = 1.0f;

			ShowWinScreen();
		}
	}

	public void TryAgain()
	{
		SceneManager.LoadScene(1, LoadSceneMode.Single);
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	private void ShowWinScreen()
	{
		int sheepHerded = SheepManager.instance.sheeps.Count;

		descriptionField.text = $"Sheep Herded: <br> {sheepHerded} / {initSheep}";

		ScoreManager.Instance.gameObject.SetActive(false);
		FindObjectOfType<UIGameOver>().gameObject.SetActive(false);

		ScoreManager.Instance.gameObject.SetActive(false);

		PlayerInput player = FindObjectOfType<PlayerInput>();
		player.enabled = false;

		player.GetComponent<CharacterMovement>().Move(Vector3.zero);
		player.GetComponent<PlayerAbilityYell>().enabled = false;
		player.GetComponent<PlayerAbilityFireCracker>().enabled = false;

		winCanvas.interactable = true;

		winCanvas.DOFade(1.0f, 1.0f)
			.SetEase(Ease.OutSine);
	}
}