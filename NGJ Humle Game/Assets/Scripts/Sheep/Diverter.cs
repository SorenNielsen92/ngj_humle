﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diverter : MonoBehaviour
{
    [Range(0,1)]
    public float DivertionValue = 0.1f;
}
