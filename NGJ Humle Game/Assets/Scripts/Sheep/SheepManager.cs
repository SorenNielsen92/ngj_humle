﻿using System.Collections.Generic;
using UnityEngine;

public class SheepManager : MonoBehaviour
{
    public int sheepsToSpawn = 10;
    public List<Sheep> sheeps = new List<Sheep>();

    public static SheepManager instance;
    public GameObject Sheep;

    public Vector3 SpawnArea = Vector3.one;
    private Vector3 ScaledSpawnArea
    {
        get
        {
            return Vector3.Scale(SpawnArea, transform.localScale);
        }
    }

    private void Awake()
    {
        instance = this;

        //Spawn sheeps
        int quadrants = Mathf.CeilToInt(Mathf.Sqrt(sheepsToSpawn));
        Vector3 quadrantSize = ScaledSpawnArea / quadrants;
        Vector3 startLocation = quadrantSize / 2;
        startLocation -= quadrantSize * quadrants / 2;
        startLocation += Vector3.Scale(transform.position, new Vector3(1, 0, 1));
        for (int i = 0; i < sheepsToSpawn; i++)
        {
            Vector3 spawnLocation = new Vector3((i % quadrants) * quadrantSize.x, 0, (Mathf.CeilToInt(i / quadrants) * quadrantSize.z));
            spawnLocation += startLocation;
            SpawnSheep(spawnLocation, quadrantSize, i);
        }
    }

    private void Update()
    {
        if (sheeps.Count > 0)
        {
            Vector4[] pos = new Vector4[sheeps.Count];
            for (int i = 0; i < sheeps.Count; i++)
            {
                pos[i] = sheeps[i].transform.position;
            }

            Shader.SetGlobalInt("SHEEPS", pos.Length);
            Shader.SetGlobalVectorArray("SHEEP_POS", pos);
        }
    }

    public void SpawnSheep(Vector3 location, Vector3 Size, int index)
    {
        Vector3 halfSpawnArea = Size * 0.25f;
        float x = Random.Range(-halfSpawnArea.x, halfSpawnArea.x);
        float z = Random.Range(-halfSpawnArea.y, halfSpawnArea.y);

        Vector3 pos = new Vector3(x, 0, z) + location;

        GameObject sheepGO = Instantiate(Sheep, pos, Quaternion.identity);
        sheepGO.transform.name += "_" + index.ToString();

        Sheep sheep = sheepGO.GetComponent<Sheep>();
        sheep.Spawn();
        sheeps.Add(sheep);
    }

    public void CreateFearZone(Vector3 location, float radius)
    {
        foreach (Sheep s in sheeps)
        {
            s.InvokeFear(location, radius);
        }
    }
    public void KillSheep(Sheep s)
    {
        GlobalEvents.OnSheepDied?.Invoke(s);
        s.Kill();
        sheeps.Remove(s);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, ScaledSpawnArea);
    }
}
