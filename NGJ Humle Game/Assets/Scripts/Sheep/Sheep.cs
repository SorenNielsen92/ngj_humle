﻿using DG.Tweening;
using System.Collections;
using UnityEngine;

public class Sheep : MonoBehaviour
{
    [SerializeField]
    private float fearDuration = 1.0f;

    public float viewDistance = 3f;

    [SerializeField]
    private GameObject smokeParticle;

    private float viewAngle = 60;
    private int viewSamples = 6;

    private Vector3 wantedDir;

    private bool saved = false;

    private Transform LastFound;

    private CharacterMovement moveController;
    private CharacterAnimationController animController;
    private AnimationEventController animEvents;

    private Coroutine fearRoutine;
    private Sheep currentEnemy;
    private GameObject instantiatedSmoke;

    public bool Fighting = false;
    private bool anticipatingFight = false;
    private bool fightOwner;
    private bool fleeing;

    public void Spawn()
    {
        moveController = GetComponent<CharacterMovement>();
        animController = GetComponentInChildren<CharacterAnimationController>();
        animEvents = animController.GetComponent<AnimationEventController>();
        animEvents.OnSheepAnimationEvent += OnSheepAnimationEvent;

        wantedDir = Vector3.Normalize(new Vector3(UnityEngine.Random.Range(-1f, 1f), 0, UnityEngine.Random.Range(-1f, 1f)));

        transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);
    }

    private void Update()
    {
        if (!Fighting)
        {
            CheckDirection();
            moveController.Move(wantedDir);
        }
    }

    private void CheckDirection()
    {
        for (int i = 0; i < viewSamples; i++)
        {
            float angle = Mathf.Lerp(-viewAngle / 2, viewAngle / 2, (float)i / viewSamples);
            Vector3 viewDir = Quaternion.Euler(0, angle, 0) * wantedDir;
            Ray lookDirection = new Ray(transform.position, viewDir);

            float direction = angle > 0 ? -1 : 1;
            Vector3 newViewDir = RaycastViewDir(lookDirection, direction);
            if (newViewDir != wantedDir)
            {
                wantedDir = newViewDir;
                break;
            }
        }
    }

    private Vector3 RaycastViewDir(Ray ray, float direction)
    {
        RaycastHit hit;
        float diversionValue = 0.0f;

        if (Physics.Raycast(ray, out hit, viewDistance, 1))
        {
            if (hit.transform.GetComponent<Diverter>() != null)
            {
                diversionValue = hit.transform.GetComponent<Diverter>().DivertionValue;
            }

            if (LastFound != hit.transform)
            {
                LastFound = hit.transform;
            }

            diversionValue = 0.1f;
        }

        return Quaternion.Euler(0, diversionValue * 90 * direction, 0) * wantedDir;
    }

    public void InvokeFear(Vector3 location, float radius)
    {
        if (anticipatingFight)
        {
            CancelFighting();
            currentEnemy?.CancelFighting();
        }

        if (Vector3.Distance(location, transform.position) < radius)
        {
            wantedDir = Vector3.Normalize(Vector3.Scale(transform.position, new Vector3(1, 0, 1)) - Vector3.Scale(location, new Vector3(1, 0, 1)));

            if (fearRoutine != null)
            {
                StopCoroutine(fearRoutine);
            }

            fearRoutine = StartCoroutine(IEFear());
        }
    }

    private IEnumerator IEFear()
    {
        moveController.SetMoveModifier(3.0f);
        animController.Anim.speed = 2.0f;

        fleeing = true;

        yield return new WaitForSeconds(fearDuration);

        moveController.SetMoveModifier(1.0f);
        animController.Anim.speed = 1.0f;

        fleeing = false;
    }

    public void CancelFighting()
    {
        Fighting = false;
        currentEnemy = null;
        animController.Anim.SetBool("Fight", false);
    }

    public void Kill()
    {
        Destroy(gameObject);
    }

    [ContextMenu("Kill Sheep")]
    public void EditorKillSheep()
    {
        SheepManager.instance.KillSheep(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        Sheep sheep = null;

        if (other.transform.parent != null)
        {
            sheep = other.transform.parent.GetComponent<Sheep>();
        }

        if (sheep != null)
        {
            if (!Fighting && !sheep.Fighting && !fleeing && !sheep.fleeing && !saved && !sheep.saved)
            {
                fightOwner = true;

                Fight(sheep);
                sheep.Fight(this);
            }
        }

        if (other.transform.GetComponent<ScoreGate>())
        {
            if (!saved)
            {
                GlobalEvents.OnSheepSaved(this);
                saved = true;

                GetComponentInChildren<SpriteRenderer>().enabled = false;
                other.transform.GetComponent<ScoreGate>().SpawnNakedSheep();
            }
            else
            {
                wantedDir *= -1.0f;
            }
        }
    }

    public void Fight(Sheep sheep)
    {
        currentEnemy = sheep;

        Fighting = true;
        anticipatingFight = true;
        moveController.Move(Vector3.zero);
        animController.Anim.SetBool("Fight", true);

        Vector3 dir = sheep.transform.position - transform.position;
        animController.UpdateCharacterDirection(dir.normalized);
    }

    private void OnSheepAnimationEvent(SheepAnimationEvent evt)
    {
        switch (evt)
        {
            case SheepAnimationEvent.AttackBegin:
                break;

            case SheepAnimationEvent.Attack:
                anticipatingFight = false;
                Vector3 dir = currentEnemy.transform.position - transform.position;
                transform.DOMove(transform.position + dir * 0.5f, 0.3f)
                    .SetEase(Ease.OutCubic)
                    .OnComplete(OnSheepHeadButt);
                break;

            case SheepAnimationEvent.AttackEnd:
                break;
        }
    }

    private void OnSheepHeadButt()
    {
        if (fightOwner)
        {
            instantiatedSmoke = Instantiate(smokeParticle, transform.position, Quaternion.identity);
            StartCoroutine(IEWaitForFight());
        }
    }

    private IEnumerator IEWaitForFight()
    {
        yield return new WaitForSeconds(2.0f);

        if (fightOwner)
        {
            Destroy(instantiatedSmoke);
        }

        if (Random.value < 0.5f)
        {
            SheepManager.instance.KillSheep(currentEnemy);

            Fighting = false;
            currentEnemy = null;
            animController.Anim.SetBool("Fight", false);
        }
        else
        {
            currentEnemy.Fighting = false;
            currentEnemy.currentEnemy = null;
            currentEnemy.animController.Anim.SetBool("Fight", false);

            SheepManager.instance.KillSheep(this);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < viewSamples; i++)
        {
            float angle = Mathf.Lerp(-viewAngle / 2, viewAngle / 2, (float)i / viewSamples);
            Vector3 viewDir = Quaternion.Euler(0, angle, 0) * wantedDir;
            Gizmos.DrawLine(transform.position, transform.position + viewDir * viewDistance);
        }
    }
}