﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterAnimationController : MonoBehaviour
{
    private Direction currentDirection;

    public Animator Anim { get;
        private set; }

    private void Awake()
    {
        Anim = GetComponent<Animator>();
    }

    public void UpdateCharacterDirection(Vector3 direction)
    {
        direction = new Vector3(direction.x, 0.0f, direction.z);
        currentDirection = GetDirectionTwoWay(direction);

        Anim.SetFloat("Direction", (int)currentDirection);

        if (direction.magnitude > 0.1f)
        {
            Anim.SetBool("FacingRight", direction.x > 0.0f);
        }

        if (direction.magnitude > 0.75f)
        {
            Anim.SetBool("Moving", true);
        }
        else
        {
            Anim.SetBool("Moving", false);
        }
    }

    public Direction GetDirectionTwoWay(Vector3 direction)
    {
        Direction dir = currentDirection;

        if (direction.x == 0.0f)
        {
            return dir;
        }

        dir = direction.x > 0.0f ? Direction.Right : Direction.Left;

        return dir;
    }

    public Direction GetDirectionFourWay(Vector3 direction)
    {
        Direction dir = currentDirection;

        if (direction.x == 0.0f && direction.z == 0.0f)
        {
            return dir;
        }

        if (Mathf.Abs(direction.z) > Mathf.Abs(direction.x))
        {
            float dot = Vector3.Dot(Vector3.forward, direction.normalized);
            dir = dot > 0 ? Direction.Up : Direction.Down;
        }
        else
        {
            dir = direction.x > 0 ? Direction.Right : Direction.Left;
        }

        return dir;
    }
}

public enum Direction
{
    Up = 0,
    Down = 1,
    Left = 2,
    Right = 3,
}