﻿using System;
using UnityEngine;
using System.Collections;

public class AnimationEventController : MonoBehaviour 
{
	private AudioSource audioSource;
	public Action<PlayerAnimationEvent> OnPlayerAnimationEvent;
	public Action<SheepAnimationEvent> OnSheepAnimationEvent;
	public Action<PugaAnimationEvent> OnPugaAnimationEvent;

	private void Awake()
    {
		audioSource = gameObject.AddComponent<AudioSource>();
    }

    public void InvokeAnimationEvent(PlayerAnimationEvent evt)
	{
		OnPlayerAnimationEvent?.Invoke(evt);
	}

	public void InvokeSheepAnimationEvent(SheepAnimationEvent evt)
	{
		OnSheepAnimationEvent?.Invoke(evt);
	}

	public void InvokePugaAnimationEvent(PugaAnimationEvent evt)
	{
		OnPugaAnimationEvent?.Invoke(evt);
	}

	public void Play_Clip(AudioClip clip)
    {
		audioSource.volume = 1;
		audioSource.PlayOneShot(clip);
    }
    public void Stop_Clip(float fadeoutTime)
    {
		StartCoroutine(FadeOut(audioSource, fadeoutTime));
    }

    IEnumerator FadeOut(AudioSource a, float fadeoutTime)
    {
		float time = 0;
        while (time < fadeoutTime)
        {
			a.volume = 1-(time / fadeoutTime);
			time += Time.deltaTime;
			yield return null;
        }
		a.Stop();
    }
}

public enum PlayerAnimationEvent
{
	YellBegin = 0,
	YellMiddle = 1,
	YellEnd = 2,

	ThrowStart = 3,
	ThrowMiddle = 4,
	ThrowEnd = 5,
}

public enum SheepAnimationEvent
{
	AttackBegin = 0,
	Attack = 1,
	AttackEnd = 2,
}

public enum PugaAnimationEvent
{
	Bark = 0,
}