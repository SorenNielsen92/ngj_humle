﻿using UnityEngine;
using System.Collections;

public class WaterKillZone : MonoBehaviour
{
    [SerializeField]
    private LayerMask whatToKill;

    [SerializeField]
    private GameObject splashParticle;

    private void OnTriggerEnter(Collider other)
    {
        if (Contains(whatToKill, other.gameObject.layer))
        {
            PlayerInput playerInput = other.transform.parent.GetComponent<PlayerInput>();

            if (playerInput != null)
            {
                playerInput.enabled = false;

                playerInput.GetComponent<CharacterMovement>().Move(Vector3.zero);
                playerInput.GetComponent<PlayerAbilityYell>().enabled = false;
                playerInput.GetComponent<PlayerAbilityFireCracker>().enabled = false;

                FindObjectOfType<CameraController>().SetTarget(null);
                FindObjectOfType<UIGameOver>().ShowGameOver("Vikings can't swim");

                Instantiate(splashParticle, new Vector3(other.transform.position.x, 0.0f, other.transform.position.z), splashParticle.transform.rotation);
                return;
            }

            Sheep sheep = other.transform.parent.GetComponent<Sheep>();

            if (sheep != null)
            {
                Instantiate(splashParticle, new Vector3(other.transform.position.x, 0.0f, other.transform.position.z), splashParticle.transform.rotation);
                StartCoroutine(DelayedKillSheep(sheep));
            }

        }
    }

    private bool Contains(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }

    private IEnumerator DelayedKillSheep(Sheep sheep)
    {
        yield return new WaitForSeconds(1.0f);
        SheepManager.instance.KillSheep(sheep);
    }
}