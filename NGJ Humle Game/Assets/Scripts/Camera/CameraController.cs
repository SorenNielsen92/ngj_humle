﻿using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance;

    [SerializeField]
    private Transform target;

    [SerializeField]
    private Vector3 targetOffset;

    [SerializeField]
    private float smoothing = 8.0f;

    private Camera cam;
    private bool shaking;

    private void Awake()
    {
        Instance = this;

        if (transform.parent != null)
        {
            transform.SetParent(null);
        }

        cam = GetComponentInChildren<Camera>();
    }

    private void FixedUpdate()
    {
        if (target == null)
        {
            return;
        }

        transform.position = Vector3.Lerp(transform.position, target.position + targetOffset, Time.deltaTime * smoothing);
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public void Shake(ShakeStrength strength)
    {
        if (shaking)
        {
            return;
        }

        shaking = true;

        switch (strength)
        {
            case ShakeStrength.Tiny:
                cam.DOShakeRotation(duration: 0.35f, strength: 1, vibrato: 70, randomness: 90, fadeOut: true)
                    .OnComplete(() => shaking = false);
                break;

            case ShakeStrength.Normal:
                cam.DOShakeRotation(duration: 0.5f, strength: 3, vibrato: 70, randomness: 90, fadeOut: true)
                    .OnComplete(() => shaking = false);
                break;

            case ShakeStrength.Strong:
                cam.DOShakeRotation(duration: 0.75f, strength: 6, vibrato: 70, randomness: 90, fadeOut: true)
                    .OnComplete(() => shaking = false);
                break;

            case ShakeStrength.EXTREME:
                cam.DOShakeRotation(duration: 1.0f, strength: 9, vibrato: 70, randomness: 90, fadeOut: true)
                    .OnComplete(() => shaking = false);
                break;
        }
    }
}

public enum ShakeStrength
{
    Tiny = 0,
    Normal = 1,
    Strong = 2,
    EXTREME = 3,
}