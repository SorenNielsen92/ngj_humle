﻿using System;

public static class GlobalEvents 
{
    //Sheep Events
    public static Action<Sheep> OnSheepDied;
    public static Action<Sheep> OnSheepSaved;
}